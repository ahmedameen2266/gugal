/*
 *     SetupSelectSerpActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import com.porg.gugal.Global
import com.porg.gugal.Material3Settings.Companion.RadioSetting
import com.porg.gugal.Material3SetupWizard
import com.porg.gugal.R
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.cse.GoogleCseSerp
import com.porg.gugal.providers.searx.SearXSerp
import com.porg.gugal.ui.theme.GugalTheme

class SetupSelectSerpActivity : ComponentActivity() {
    @OptIn(
        ExperimentalMaterialApi::class,
        androidx.compose.animation.ExperimentalAnimationApi::class,
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val currentSerpProviderId = mutableStateOf("")
        setContent {
            GugalTheme {
                val _csp_id by currentSerpProviderId
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Material3SetupWizard.TwoButtons(
                        positiveAction = {
                            setSerpProvider(_csp_id)
                            val intent = Intent(applicationContext, SetupConfigureSerpActivity::class.java)
                            intent.component =
                                ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupConfigureSerpActivity")
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        },
                        positiveText = "Next",
                        negativeAction = {
                            finish()
                        }
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3SetupWizard.Header(
                            text = getText(R.string.setup_p2_title).toString(),
                            doFinish = { finish() }
                        )
                        Global.allSerpProviders.forEach { serpID ->
                            if (_csp_id == "") {
                                currentSerpProviderId.value = serpID
                            }
                            val info: ProviderInfo = getInfo(serpID)!!
                            RadioSetting(
                                title = info.name,
                                body = info.description,
                                selected = _csp_id == serpID,
                                onClick = {
                                    currentSerpProviderId.value = serpID
                                }
                            )
                        }
                    }
                }
            }
        }
    }

    // TODO if adding SERP providers: add provider in these 2 functions
    private fun setSerpProvider(serpID: String) {
        if (serpID == GoogleCseSerp.id) Global.serpProvider = GoogleCseSerp()
        else if (serpID == SearXSerp.id) Global.serpProvider = SearXSerp()
        // Check if serpID matches your SERP provider's ID, and if so set Global.serpProvider to
        // an instance of your SERP provider.
    }
    private fun getInfo(serpID: String): ProviderInfo? {
        if (serpID == GoogleCseSerp.id) return GoogleCseSerp.providerInfo
        else if (serpID == SearXSerp.id) return SearXSerp.providerInfo
        // Check if serpID matches your SERP provider's ID, and if so return your SERP provider's
        // provider info.
        return null
    }
}