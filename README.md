# Gugal

Alternative Google Search app built with Jetpack Compose, using Google CSE.

## Download

Gugal can be downloaded from [the GitLab releases page](https://gitlab.com/narektor/gugal/-/releases), and is also available on F-Droid.

Download from one _or_ the other. The versions are incompatible as F-Droid signs apps themselves.

## Set up

To use Gugal you need a CSE ID and API key. Instructions for obtaining both can be found in the app.

**To get these values you must have a Google account, therefore Google might still link searches to your account.**
