/*
 *     ChangelogActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.porg.gugal.ui.theme.GugalTheme

class ChangelogActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TODO If you forked Gugal please change these URLs to show the changelog for your fork
        val url =
            if (BuildConfig.VERSION_NAME.contains(".b")) "https://gitlab.com/gugal/beta/-/raw/main/changelogs/${BuildConfig.VERSION_CODE}.txt"
            else "https://gitlab.com/narektor/gugal/-/raw/main/fastlane/metadata/android/en-US/changelogs/${BuildConfig.VERSION_CODE}.txt"
        val changelog = mutableStateOf("Loading change list...")
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Material3SetupWizard.TwoButtons(
                        positiveAction = { finish() },
                        positiveText = "Close",
                        negativeAction = null
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Material3SetupWizard.Header(
                            text = getText(R.string.changelog_title).toString(),
                            doFinish = { finish() },
                            showBackButton = false
                        )
                        val _change by changelog
                        Text(
                            text = _change,
                            modifier = Modifier
                                .padding(start = 24.dp, top = 0.dp, bottom = 24.dp, end = 24.dp)
                                .fillMaxWidth(),
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                }
            }
        }
        if (BuildConfig.VERSION_NAME.contains("pr-") || BuildConfig.VERSION_NAME.contains("ci-")) {
            changelog.value = "This version doesn't have a change list."
        } else {
            // Instantiate the RequestQueue.
            val queue = Volley.newRequestQueue(this)

            // Request a string response from the provided URL.
            val stringRequest = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    changelog.value = response
                },
                Response.ErrorListener { changelog.value = "Error loading change list. Please check your Internet connection." })

            // Add the request to the RequestQueue.
            queue.add(stringRequest)
        }
    }
}