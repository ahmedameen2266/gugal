/*
 *     SettingsPage.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.porg.gugal.Material3Settings.Companion.RegularSetting

@Composable
fun SettingsPage(context: Context?) {
    Column() {
        RegularSetting("Donate", "If you like Gugal, " +
                "you can donate to me on Ko-fi. Tap here to open the donation page.",
            onClick = {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://ko-fi.com/thegreatporg")
                )
                startActivity(context!!, Intent.createChooser(intent, "Open in"), null)
            })
        RegularSetting("Found an issue?",
            "You can report issues on GitLab (requires a GitLab account). Tap here to open it.",
            onClick = {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://gitlab.com/narektor/gugal/-/issues/new")
                )
                startActivity(context!!, Intent.createChooser(intent, "Open in"), null)
            })
        RegularSetting("View source code",
            "Tap here to open the source code of Gugal on GitLab.",
            onClick = {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://gitlab.com/narektor/gugal")
                )
                startActivity(context!!, Intent.createChooser(intent, "Open in"), null)
            })
        RegularSetting("What's new",
            "Learn what's new in this version of Gugal.",
            onClick = {
                val intent = Intent(context, ChangelogActivity::class.java)
                intent.component =
                    ComponentName("com.porg.gugal", "com.porg.gugal.ChangelogActivity")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context!!, intent, null)
            })
        RegularSetting("Change credentials",
            "Opens the current search engine's set-up page.",
            onClick = {
                val intent = Intent(context, ChangelogActivity::class.java)
                intent.component =
                    ComponentName("com.porg.gugal", "com.porg.gugal.setup.SetupConfigureSerpActivity")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("launchedFromSettings", true)
                startActivity(context!!, intent, null)
            })
        Text(
            text = "Gugal " + BuildConfig.VERSION_NAME,
            modifier = Modifier.padding(start = 20.dp),
            style = MaterialTheme.typography.bodyMedium
        )
    }

}