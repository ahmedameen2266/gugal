/*
 *     Result.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

/**
 * A search result.
 *
 * @param title the page's title, shown first and large.
 * @param body an optional snippet from the page shown as the result's body.
 * If none was found or the search engine doesn't support them set to null.
 * @param url the page's URL, will be opened when the result is tapped.
 * @param domain the page's domain, shown in small text below the title.
 */
data class Result(val title: String, val body: String?, val url: String, val domain: String)