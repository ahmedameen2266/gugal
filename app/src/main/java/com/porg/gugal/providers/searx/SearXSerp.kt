/*
 *     SearXSerp.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers.searx

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.porg.gugal.Result
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.providers.exceptions.InvalidCredentialException
import com.porg.gugal.providers.exceptions.SearchException
import org.json.JSONObject
import java.net.URI
import java.net.URL

class SearXSerp: SerpProvider {

    private var url = ""

    @Composable
    override fun ConfigComposable(modifier: Modifier) {
        val _url = remember { mutableStateOf(TextFieldValue()) }
        Column(
            modifier = Modifier
                .padding(all = 4.dp)
                .then(modifier)
        ) {
            TextField(
                placeholder = { Text(text = "Instance") },
                value = _url.value,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth(),
                onValueChange = { nv ->
                    _url.value = nv
                    url = _url.value.text
                },
                keyboardActions = KeyboardActions(
                    onDone = {
                        url = _url.value.text
                    }
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Done
                )
            )
            Text(
                text = "The instance must have API access enabled. Most public instances have it disabled, and it's recommended to use a self-hosted SearX(NG) instance for that reason.",
                modifier = Modifier.padding(all = 4.dp),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }

    override fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("url" to url)
    }

    override fun getSensitiveCredentialNames(): Array<String> {
        return arrayOf("url")
    }

    override fun useSensitiveCredentials(credentials: Map<String, String>) {
        if (!credentials.containsKey("url")) throw InvalidCredentialException()
        url = credentials["url"]!!
    }

    override fun search(query: String, resultList: SnapshotStateList<Result>): JsonObjectRequest? {
        if (!url.contains("https")) url = "https://$url"
        val uri: URL = URI.create(url).toURL()
        val fullUrl: String = uri.protocol.toString() + "://" + uri.authority + "/?q=$query&format=json&lang=en&time_range=day"
        // Request a string response from the provided URL.
        return JsonObjectRequest(
            Request.Method.GET, fullUrl, null,
            { response ->
                val items = response.getJSONArray("results")
                resultList.clear()
                for (i in 0 until items.length()) {
                    val item: JSONObject = items.getJSONObject(i)
                    // get parsed URL
                    val pars = item.getJSONArray("parsed_url")
                    // show the engine name in the domain too
                    val domain = "${pars[1]} (via ${item.getString("engine")})"
                    if (item.has("content"))
                        resultList.add(Result(item.getString("title"), item.getString("content"),
                            item.getString("url"), domain))
                    else
                        resultList.add(Result(item.getString("title"), null,
                            item.getString("url"), domain))
                }
            },
            { error ->
                Log.e("SearXSerp", "Error!")
                //val status = error.networkResponse.statusCode
                // check for 4xx error codes, not all instances return 403
                //if (status in 400..499) throw InvalidCredentialException()
                error.message?.let {
                    resultList.add(Result("Error", it, "",""))
                    throw SearchException(it)
                }
            }
        )
    }

    override val providerInfo = Companion.providerInfo
    override val id: String get() = Companion.id

    companion object {
        val id: String = "198debd01044412d8f2c22d9e2ae9e8d-searx"
        val providerInfo = ProviderInfo(
            "SearX/SearXNG",
            "Privacy-respecting metasearch engine.",
            "SearX")
    }
}