/*
 *     Material3SetupWizard.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

/**
 * Contains Material 3 components for setup wizards, which are similar to Pixel Setup on Android 12 and above.
 */
class Material3SetupWizard {

    companion object {
        /**
         * A header with large text and optionally a Back button.
         *
         * @param text the title.
         * @param doFinish the action to do when the Back button is pressed, if the Back button is enabled. Can be null if the Back button isn't enabled.
         * @param showBackButton whether to show the Back button. Defaults to true.
         */
        @Composable
        fun Header(text: String, doFinish: (() -> Unit)?, showBackButton: Boolean = true) {
            if (showBackButton)
                IconButton(
                    onClick  = doFinish!!,
                    modifier = Modifier.
                    then(Modifier.padding(start = 16.dp, top = 16.dp, bottom = 0.dp, end = 16.dp))
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Go back",
                    )
                }
            Text(
                text = text,
                modifier = Modifier
                    .padding(start = 24.dp, top = 72.dp, bottom = 24.dp, end = 24.dp)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.displaySmall
            )
        }

        /**
         * A layout with a negative (outlined, on the left) and positive (filled, on the right) button.
         *
         * @param positiveAction called when the positive button is pressed.
         * @param positiveText the text for the positive button.
         * @param negativeAction called when the negative button is pressed. Can be null, in which case the negative button isn't shown.
         * @param negativeText the text for the negative button. Defaults to "Back".
         */
        @Composable
        fun TwoButtons(positiveAction: () -> Unit, positiveText: String, negativeAction: (() -> Unit)?, negativeText: String = "Back") {
            Box(
                modifier = Modifier.fillMaxSize().then(PaddingModifier),
                Alignment.BottomCenter
            ) {
                Surface(modifier = Modifier.fillMaxWidth(),color = MaterialTheme.colorScheme.background, tonalElevation = 0.dp) {
                    Box (modifier = Modifier.fillMaxWidth()) {
                        Button(
                            modifier = Modifier.padding(all = 4.dp).align(Alignment.BottomEnd),
                            onClick = positiveAction
                        ) {
                            Text(positiveText)
                        }
                        if (negativeAction != null) {
                            OutlinedButton(
                                modifier = Modifier.padding(all = 4.dp).align(Alignment.BottomStart),
                                onClick = negativeAction
                            ) {
                                Text(negativeText)
                            }
                        }
                    }
                }
            }
        }

        /**
         * A tip - a round surface with an icon and text.
         *
         * @param text the tip's text.
         * @param modifier an extra modifier applied to the surface.
         * @param icon the icon's resource ID.
         * @param onClick what to do when the tip is clicked. Can be null.
         */
        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun Tip(text: String, modifier: Modifier, icon: Int, onClick: (() -> Unit)?) {
            if (onClick != null) {
                Surface(
                    modifier = modifier,
                    shape = RoundedCornerShape(20.dp),
                    tonalElevation = 2.dp,
                    onClick = onClick
                ) {TipContent(text, icon)}
            } else {
                Surface(
                    modifier = modifier,
                    shape = RoundedCornerShape(20.dp),
                    tonalElevation = 2.dp
                ) {TipContent(text, icon)}
            }
        }

        @Composable
        private fun TipContent(text: String, image: Int) {
            Row(
                modifier = Modifier.padding(all = 14.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painterResource(image),
                    modifier = Modifier.size(36.dp),
                    colorFilter = ColorFilter.tint(
                        MaterialTheme.colorScheme.secondary
                    ),
                    contentDescription = ""
                )
                Text(
                    text = text,
                    modifier = Modifier.padding(start = 14.dp),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }

        /**
         * A tip - a round surface with an icon and text.
         *
         * @param text the tip's text.
         * @param modifier an extra modifier applied to the surface.
         * @param icon the icon's resource ID.
         */
        @Composable
        fun Tip(text: String, modifier: Modifier, image: Int) {
            Tip(text, modifier, image, null)
        }

        /**
         * A padding modifier used in the setup elements.
         *
         * Can be applied to other UI elements to fit in with ones provided by this class.
         */
        val PaddingModifier: Modifier = Modifier.padding(16.dp)
    }
}